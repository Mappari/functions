function allDigits(str){
  for(let char of str){
    if(char < "0" || char > "9"){
      return false;
    }
  }
  return true;
}

const getSum = (str1, str2) => {
  if(typeof str1 != "string" || typeof str2 != "string" || !allDigits(str1) || !allDigits(str2)){
    return false;
  } 
  if(str1.length > str2.length){
    str2 = new Array(str1.length - str2.length).fill("0").join("") + str2;
  }
  else if(str2.length > str1.length){
    str1 = new Array(str2.length - str1.length).fill("0").join("") + str1;
  }
  let buffer = 0;
  let result = "";
  for(let i = str1.length - 1; i >= 0; i--){
    let num = parseInt(str1[i]) + parseInt(str2[i]) + buffer;
    result = num%10 + result;
    buffer = Math.floor(num/10);
  }
  if(buffer != 0){
    result = buffer + result;
  }
  return result;
}

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let postAmount = 0, commentsAmount = 0;
  for(let post of listOfPosts){
    if(post.author == authorName){
      postAmount++;
    }
    if(post.hasOwnProperty("comments")){
      for(let comment of post.comments){
        if(comment.author == authorName){
          commentsAmount++;
        }
      }
    }
  }
  return "Post:" + postAmount + ",comments:" + commentsAmount;
}

const tickets=(people)=> {
  let bank = 0;
  for(let human of people){
    if(bank >= human-25){
      bank += 25;
    }
    else {
      return "NO";
    }
  }
  return "YES";
}


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
